package main

import (
	"fmt"
	"leetcode"
)

func main() {
	grid := [][]byte{
		{'1', '1', '0', '0', '0'},
		{'1', '1', '0', '0', '0'},
		{'0', '0', '1', '0', '0'},
		{'0', '0', '0', '1', '1'},
	}
	r := leetcode.NumIslands(grid)
	fmt.Println(r)
}

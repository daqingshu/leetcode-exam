package main

import (
	"fmt"
	"leetcode"
)

func main() {
	node5 := &leetcode.ListNode{Val: 5, Next: nil}
	node4 := &leetcode.ListNode{Val: 4, Next: node5}
	node3 := &leetcode.ListNode{Val: 3, Next: node4}
	node2 := &leetcode.ListNode{Val: 2, Next: node3}
	node1 := &leetcode.ListNode{Val: 1, Next: node2}
	fmt.Println(node1.Count())
	k := 200001
	l := leetcode.RotateRight(node1, k)
	l.Print()
}

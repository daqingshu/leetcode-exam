package main

import (
	"fmt"
	"leetcode"
)

func main() {
	// [10,5,-3,3,2,null,11,3,-2,null,1]
	node9 := &leetcode.TreeNode{Val: 3, Left: nil, Right: nil}
	node8 := &leetcode.TreeNode{Val: -2, Left: nil, Right: nil}

	node6 := &leetcode.TreeNode{Val: 3, Left: node9, Right: node8}

	node7 := &leetcode.TreeNode{Val: 1, Left: nil, Right: nil}
	node5 := &leetcode.TreeNode{Val: 2, Left: nil, Right: node7}

	node3 := &leetcode.TreeNode{Val: 5, Left: node6, Right: node5}

	node4 := &leetcode.TreeNode{Val: 11, Left: nil, Right: nil}
	node2 := &leetcode.TreeNode{Val: -3, Left: nil, Right: node4}

	node1 := &leetcode.TreeNode{Val: 10, Left: node3, Right: node2}

	sum := 8
	height := leetcode.PathSum3(node1, sum)
	fmt.Println()
	fmt.Println(height)
}

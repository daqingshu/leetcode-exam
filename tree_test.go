package leetcode

import "testing"

func Test_MinDepth(t *testing.T) {
	type args struct {
		root *TreeNode
	}

	node9 := &TreeNode{Val: 9, Left: nil, Right: nil}

	node15 := &TreeNode{Val: 15, Left: nil, Right: nil}
	node7 := &TreeNode{Val: 7, Left: nil, Right: nil}
	node20 := &TreeNode{Val: 20, Left: node15, Right: node7}

	node3 := &TreeNode{Val: 3, Left: node9, Right: node20}
	tests := []struct {
		name string
		args args
		want int
	}{
		// TODO: Add test cases.
		{name: "a",
			args: args{node3},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := MinDepth(tt.args.root); got != tt.want {
				t.Errorf("minDepth() = %v, want %v", got, tt.want)
			}
		})
	}
}

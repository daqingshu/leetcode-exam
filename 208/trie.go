package main

import (
	"fmt"
	"leetcode"
)

func main() {
	trie := leetcode.Constructor()

	trie.Insert("apple")

	ret := trie.Search("apple") // 返回 true
	fmt.Println(ret)
	ret = trie.Search("app") // 返回 false
	fmt.Println(ret)
	ret = trie.StartsWith("app") // 返回 true
	fmt.Println(ret)
	trie.Insert("app")
	ret = trie.Search("app") // 返回 true
	fmt.Println(ret)
}

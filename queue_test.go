package leetcode

import (
	"testing"
)

func TestQueue(t *testing.T) {
	q := NewQueue()
	q.EnQueue(1)
	q.EnQueue(2)
	q.EnQueue(4)
	var got int
	var want int
	got = q.DeQueue().(int)
	want = 1
	if got != want {
		t.Errorf("DeQueue() = %v, want %v", got, want)
	}
	got = q.DeQueue().(int)
	want = 2
	if got != want {
		t.Errorf("DeQueue() = %v, want %v", got, want)
	}
	got = q.DeQueue().(int)
	want = 4
	if got != want {
		t.Errorf("DeQueue() = %v, want %v", got, want)
	}
}

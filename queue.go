package leetcode

import "container/list"

// Queue Queue is FIFO
type Queue struct {
	l *list.List
}

// NewQueue create new queue
func NewQueue() *Queue {
	l := list.New()
	return &Queue{
		l,
	}
}

// EnQueue put an element to queue
func (q *Queue) EnQueue(v interface{}) {
	q.l.PushBack(v)
}

// DeQueue remove the first element from queue
func (q *Queue) DeQueue() interface{} {
	e := q.l.Front()
	return q.l.Remove(e)
}

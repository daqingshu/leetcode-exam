package leetcode

import (
	"reflect"
	"testing"
)

func Test_largestRectangleArea(t *testing.T) {
	type args struct {
		t []int
	}
	a1 := []int{2, 1, 2}
	a2 := []int{2, 1, 5, 6, 2, 3}
	a3 := []int{5, 3, 2, 2, 3}
	a4 := []int{1, 2, 2}
	tests := []struct {
		name string
		args args
		want int
	}{
		// TODO: Add test cases.
		{name: "test1", args: args{a1}, want: 3},
		{name: "test2", args: args{a2}, want: 10},
		{name: "test3", args: args{a3}, want: 10},
		{name: "test4", args: args{a4}, want: 4},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := largestRectangleArea(tt.args.t); got != tt.want {
				t.Errorf("largestRectangleArea() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_prisonAfterNDays(t *testing.T) {
	type args struct {
		cells []int
		N     int
	}
	// a1 := []int{1, 0, 0, 1, 0, 0, 1, 0}
	// N1 := 100
	// r1 := []int{1, 0, 0, 1, 0, 0, 1, 0}

	a2 := []int{1, 0, 0, 1, 0, 0, 0, 1}
	N2 := 826
	r2 := []int{0, 1, 1, 0, 1, 1, 1, 0}
	tests := []struct {
		name string
		args args
		want []int
	}{
		//{name: "test1", args: args{a1, N1}, want: r1},
		{name: "test2", args: args{a2, N2}, want: r2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := PrisonAfterNDays(tt.args.cells, tt.args.N); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("prisonAfterNDays() = %v, want %v", got, tt.want)
			}
		})
	}
}

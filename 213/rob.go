package main

import (
	"fmt"
)

func main() {
	nums := []int{1, 3, 1, 3, 100}
	len := rob2(nums)
	fmt.Println(len)
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func rob(nums []int) int {
	numlen := len(nums)
	if numlen == 0 {
		return 0
	}
	sumarr := make([]int, numlen)
	for i := 0; i < numlen; i++ {
		switch i {
		case 0:
			sumarr[0] = nums[0]
		case 1:
			sumarr[1] = max(nums[0], nums[1])
		default:
			sumarr[i] = max(sumarr[i-2]+nums[i], sumarr[i-1])
		}
	}

	return sumarr[numlen-1]
}

func rob2(nums []int) int {
	numlen := len(nums)
	if numlen == 0 {
		return 0
	}
	if numlen == 1 {
		return nums[0]
	}
	if numlen == 2 {
		return max(nums[0], nums[1])
	}
	singlSum := 0
	doubleSum := 0
	singlSum = rob(nums[0 : numlen-1])
	doubleSum = rob(nums[1:numlen])

	return max(singlSum, doubleSum)
}

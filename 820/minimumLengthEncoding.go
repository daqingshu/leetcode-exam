package main

import (
	"fmt"
	"leetcode"
)

func main() {
	words := []string{"time", "atime", "btime"}
	len := minimumLengthEncoding(words)
	fmt.Println()
	fmt.Println(len)
}

func minimumLengthEncoding(words []string) int {
	trie := leetcode.Constructor()
	for i := 0; i < len(words); i++ {
		trie.Insert(words[i])
	}
	alllen := trie.ToString()
	fmt.Println(alllen)
	return alllen
}

package main

import (
	"fmt"
	"leetcode"
)

func main() {
	// [3 2 3 nil 3 1 nil]
	node5 := &leetcode.TreeNode{Val: 1, Left: nil, Right: nil}
	node4 := &leetcode.TreeNode{Val: 3, Left: nil, Right: nil}

	node3 := &leetcode.TreeNode{Val: 3, Left: nil, Right: node5}
	node2 := &leetcode.TreeNode{Val: 2, Left: nil, Right: node4}
	node1 := &leetcode.TreeNode{Val: 3, Left: node2, Right: node3}

	height := rob(node1)

	fmt.Println(height)
}

func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func rob(root *leetcode.TreeNode) int {
	return pathSum3(root)
}

func pathSum3(node *leetcode.TreeNode) int {
	if node == nil {
		return 0
	}

	lrob := pathSum3(node.Left)
	rrob := pathSum3(node.Right)

	return max(node.Val, lrob+rrob)
}

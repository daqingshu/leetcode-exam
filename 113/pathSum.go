package main

import (
	"fmt"
	"leetcode"
)

func main() {
	// [10,5,-3,3,2,null,11,3,-2,null,1]
	node9 := &leetcode.TreeNode{Val: 7, Left: nil, Right: nil}
	node8 := &leetcode.TreeNode{Val: 2, Left: nil, Right: nil}

	node10 := &leetcode.TreeNode{Val: 5, Left: nil, Right: nil}
	node7 := &leetcode.TreeNode{Val: 1, Left: nil, Right: nil}
	node6 := &leetcode.TreeNode{Val: 13, Left: nil, Right: nil}
	node5 := &leetcode.TreeNode{Val: 4, Left: node10, Right: node7}

	node3 := &leetcode.TreeNode{Val: 8, Left: node6, Right: node5}

	node4 := &leetcode.TreeNode{Val: 11, Left: node9, Right: node8}
	node2 := &leetcode.TreeNode{Val: 4, Left: node4, Right: nil}

	node1 := &leetcode.TreeNode{Val: 5, Left: node2, Right: node3}

	sum := 22
	height := leetcode.PathSum2(node1, sum)
	fmt.Println()
	fmt.Println(height)
}

package main

import "testing"

func Test_ladderLength(t *testing.T) {
	type args struct {
		beginWord string
		endWord   string
		wordList  []string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		// TODO: Add test cases.
		{name: "test1", args: args{"hit", "cog", []string{"hot", "dot", "dog", "lot", "log", "cog"}}, want: 5},
		{name: "test2", args: args{"hot", "dog", []string{"hot", "dot", "dog"}}, want: 3},
		{name: "test3", args: args{"a", "c", []string{"a", "b", "c"}}, want: 2},
		{name: "test4", args: args{"red", "tax", []string{"ted", "tex", "red", "tax", "tad", "den", "rex", "pee"}}, want: 4},
		{name: "test5", args: args{"hot", "dog", []string{"hot", "dog"}}, want: 0},
		{name: "test6", args: args{"hit", "cog", []string{"hot", "dot", "tog", "cog"}}, want: 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ladderLength(tt.args.beginWord, tt.args.endWord, tt.args.wordList); got != tt.want {
				t.Errorf("ladderLength() = %v, want %v", got, tt.want)
			}
		})
	}
}

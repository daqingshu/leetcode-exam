package main

import (
	"fmt"
	"leetcode"
)

func main() {
	cells := []int{1, 0, 0, 1, 0, 0, 1, 0}
	N := 100
	height := leetcode.PrisonAfterNDays(cells, N)
	fmt.Println(height)
}

package main

import (
	"fmt"
	"leetcode"
)

func main() {
	in := []int{1, 2, 3, 4, 5, 6, 7}
	k := 3
	leetcode.Rotate(in, k)
	fmt.Println(in)
}

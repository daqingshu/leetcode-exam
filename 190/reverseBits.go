package main

import (
	"fmt"
	"leetcode"
	"strconv"
)

func main() {
	binary := "00000010100101000001111010011100"
	fmt.Printf("input: \n%v\n", binary)
	decimal, err := strconv.ParseUint(binary, 2, 32)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("input: %v\n", decimal)
	r := leetcode.ReverseBits(uint32(decimal))
	fmt.Println(r)
}

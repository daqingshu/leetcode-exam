package main

import (
	"fmt"
	"leetcode"
)

func main() {
	//[3,9,20,null,null,15,7],
	node9 := &leetcode.TreeNode{Val: 9, Left: nil, Right: nil}

	node15 := &leetcode.TreeNode{Val: 15, Left: nil, Right: nil}
	node7 := &leetcode.TreeNode{Val: 7, Left: nil, Right: nil}
	node20 := &leetcode.TreeNode{Val: 20, Left: node15, Right: node7}

	node3 := &leetcode.TreeNode{Val: 3, Left: node9, Right: node20}
	fmt.Println(node3)
	node2 := &leetcode.TreeNode{Val: 2, Left: nil, Right: nil}
	node1 := &leetcode.TreeNode{Val: 1, Left: node2, Right: nil}
	fmt.Println(node1)

	node4 := &leetcode.TreeNode{Val: 4, Left: nil, Right: nil}
	node5 := &leetcode.TreeNode{Val: 5, Left: nil, Right: nil}

	//[1,2,3,4,null,null,5]
	node2 = &leetcode.TreeNode{Val: 2, Left: node4, Right: nil}
	node3 = &leetcode.TreeNode{Val: 3, Left: nil, Right: node5}
	node1 = &leetcode.TreeNode{Val: 1, Left: node2, Right: node3}

	height := leetcode.MinDepth(node1)

	fmt.Println(height)
}
